The main idea of the project is to list trending projects on Github and when tap on one of them, show their details.
The deisgn pattern which has been followed into this project is MVVM - Swift 4.2.

URLs which are used are as follows:

1. Listing Trending projects: URL - GET https://github-trending-api.now.sh/repositories
2. Fetch Repository contents API: URL - GET https://api.github.com/repos/"author"/"projectName"/contents/
3. Fetcg README content: gitUrl from Fetch repository contents API.
4. Image Download API.

>Searching the project is out of scope (not mentioned) of this project so the UI of UISearchController is missing.

As the project is mostly concerned with reusability, For example:

1. I have used a common <TableViewDataSource> class for loading the UITableViewCell from different cell identifier which reloads the UITableView with different content using the same View Model.

2. As every detail page consists of an image and a readMe content which are to be downloaded, I have used three NSCache<KeyType, ObjectType>. These are:
   a. contentUrlCache = NSCache<NSString, NSURL>()
   b. contentCache = NSCache<NSURL, NSString>()
   c. imageCache = NSCache<NSURL, UIImage>()
   
NSCache is used as a temporary storage for transient key-value pairs that are subject to eviction when resources are low. Cache objects differ from other mutable collections in a few ways:
1. The NSCache class incorporates various auto-eviction policies, which ensure that a cache doesn�t use too much of the system�s memory. If memory is needed by other applications, these policies remove some items from the cache, minimizing its memory footprint.
2. We can add, remove, and query items in the cache from different threads without having to lock the cache yourself.
3. Unlike an NSMutableDictionary object, a cache does not copy the key objects that are put into it.