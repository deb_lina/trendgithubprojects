//
//  GithubProjectsTableViewController.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 21/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import UIKit

class GithubProjectsTableViewController: UITableViewController {

    var projectsViewModel: ProjectsViewModel?
    var dataSource: TableViewDataSource!
    var tableViewProperty: TableViewProperty!
    var designSwapped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateTableViewProperty(with: projectsViewModel)
        initRefreshControl()
        call()
    }
    
    func populateTableViewProperty(with vm: ProjectsViewModel?) {
        tableViewProperty = TableViewProperty()
        tableViewProperty._self = self.tableView
        tableViewProperty.data = vm
        tableViewProperty.designSwapped = designSwapped
        tableViewProperty.cellIdentifier = self.designSwapped == false ? "projectCell" : "projectCell1"
        dataSource = TableViewDataSource(tableViewProperty: self.tableViewProperty)
        
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
    }
    
    
    
    func call() {
        ServiceAPI().call { (projects, error) in
            if let error = error {
                self.projectsViewModel = nil
                self.populateTableViewProperty(with: self.projectsViewModel)
                DispatchQueue.main.async {
                    AlertController.present(title: "Load Failed!", message: error.localizedDescription, okTitle: "Reload", cancelTitle: "Cancel", okAction: {
                        self.call()
                    })
                }
            } else if let projects = projects {
                // Projects View Model is initialised and re-initialised while refeshing
                self.projectsViewModel = ProjectsViewModel(projects: projects)
                self.populateTableViewProperty(with: self.projectsViewModel)
            }
        }
    }
    
    fileprivate func initRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.tintColor = UIColor.hoki()
        self.refreshControl?.addTarget(self, action:  #selector(refresh), for: .valueChanged)
    }
    
    // Refresh the view with projects
    @objc func refresh() {
        call()
    }

    
    @IBAction func swapDesignButtonTapped(_ sender: UIBarButtonItem) {
        designSwapped = !designSwapped
        populateTableViewProperty(with: self.projectsViewModel)
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
    }
}

extension GithubProjectsTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let _projectsViewModel = self.projectsViewModel else {
            return
        }
        guard let projectViewModel = _projectsViewModel.pull(at: indexPath.row) else {
            return
        }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(ProjectDetailsViewController.viewController(with: projectViewModel), animated: true)
        }
    }
}
