//
//  PhotoCollectionViewCell.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 21/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import UIKit

var imageCache = NSCache<NSURL, UIImage>()

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var authorName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    
    var authorVM: AuthorViewModel? {
        didSet {
            self.authorName.text = authorVM?.authorName
            
            // Image download and caching
            activityIndicator.startAnimating()
            imageView.image = nil
            if let imageURL = authorVM?.avatarImageUrl {
                // Fetch from cache
                if let image = imageCache.object(forKey: imageURL as NSURL) {
                    activityIndicator.stopAnimating()
                    self.imageView.image = image
                } else {
                    // Download image from url
                    authorVM?.loadAvatarImage(imageURL: imageURL, completion: { (data, error) in
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                        }
                        guard error == nil else { return }
                        let image = UIImage(data: data!)
                        imageCache.setObject(image!, forKey: imageURL as NSURL)
                        DispatchQueue.main.async {
                            self.imageView.image = image
                            self.activityIndicator.stopAnimating()
                        }
                    })
                }
            }
        }
    }
}
