//
//  ViewController.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 21/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import UIKit

class ProjectDetailsViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var readMeTextView: UITextView!
    @IBOutlet var centerView: UIView!
    @IBOutlet var starLabel: UILabel!
    @IBOutlet var forkLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var projectVM: ProjectViewModel!
    
    var readMeContentText: String?
    
    // MARK: Factory Method
    class func viewController(with projectVM: ProjectViewModel) -> ProjectDetailsViewController {
        let controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProjectDetailsViewController") as! ProjectDetailsViewController
        controller.projectVM = projectVM
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = projectVM.projectName
        collectionView.delegate = self
        collectionView.dataSource = self
        centerView.layer.borderColor = UIColor.hoki().cgColor
        centerView.layer.borderWidth = 1.0
        configureViewDetails()
        loadReadMeContent()
        self.collectionView.reloadData()
    }
    
    deinit {
        print("View Controller Deinitialised!!")
    }

    fileprivate func configureViewDetails() {
        descriptionTextView.text = projectVM.projectDescription
        starLabel.text =  projectVM.starString
        forkLabel.text = projectVM.forkString
    }
    
    fileprivate func loadReadmeFromCache(content: String) {
        DispatchQueue.main.async {
            self.readMeTextView.text = content
        }
    }

    fileprivate func loadReadMeContent() {
        
        var baseUrlString = Configuration.baseUrl.value()! as String
        baseUrlString = baseUrlString + "repos/" + projectVM.projectAuthor + "/" + projectVM.projectName + "/contents/"
        
        // Load from Cache if exists
        if let url = contentUrlCache.object(forKey: (projectVM.projectAuthor + projectVM.projectName) as NSString),
            let content = contentCache.object(forKey: url as NSURL) {
            activityIndicator.stopAnimating()
            
            loadReadmeFromCache(content: content as String)
            
        } else {
            // Load from URL
            fetchRepoContents(with: baseUrlString)
        }
        
    }
    
    fileprivate func fetchReadMe(gitURL: URL) {
        self.projectVM.fetchReadMe(url: gitURL, completion: { (readMeString, error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
            guard error == nil else {
                DispatchQueue.main.async {
                    self.readMeTextView.text = "Not Found"
                }
                return }
            contentCache.setObject(readMeString! as NSString, forKey: gitURL as NSURL)
            DispatchQueue.main.async {
                self.readMeTextView.text = readMeString
            }
        })
    }
    
    fileprivate func fetchRepoContents(with baseUrlString: String) {
        activityIndicator.startAnimating()
        projectVM.fetchRepoContents(urlString: baseUrlString) { (repoContents, error) in
            
            guard error == nil else {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.readMeTextView.text = "Not Found"
                }
                return }
            contentUrlCache.setObject(NSURL(string: baseUrlString)!, forKey: (self.projectVM.projectAuthor + self.projectVM.projectName) as NSString)
            for content in repoContents! {
                if let name = content.name, name.lowercased().contains("readme"),
                    let links = content.links,
                    let gitLink = links.gitLinkString,
                    let gitURL = URL(string: gitLink) {
                    
                    self.fetchReadMe(gitURL: gitURL)
                }
            }
        }
    }


}

extension ProjectDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projectVM.builtByCount
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionViewCell
//        guard let builtByCount = projectVM.builtByCount, builtByCount > 0 else {
//            return cell
//        }
        guard let _authorVM = projectVM.pullBuiltBy(at: indexPath.row) else {
            return cell
        }
        cell.authorVM = _authorVM
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let builtByCount = projectVM.builtByCount, builtByCount > 0 else {
            return CGSize(width: 0.0, height: 0.0)
        }
        return CGSize(width: UIScreen.main.bounds.width / CGFloat(builtByCount), height: collectionView.bounds.height)
    }
}

