//
//  ProjectOneTableViewCell.swift
//  TrendingGitHubProjects
//
//  Created by StuCred on 23/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit

class ProjectOneTableViewCell: UITableViewCell {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var projectAuthor: UILabel!
    
    
    var projectVM: ProjectViewModel? {
        didSet {
            if let _name = projectVM?.projectName {
                name.text = _name
            }
            if let _projectAuthor = projectVM?.projectAuthor {
                projectAuthor.text = "Author\n" + _projectAuthor
            }
        }
    }
    
}

