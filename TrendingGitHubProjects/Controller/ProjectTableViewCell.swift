//
//  ProjectTableViewCell.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 21/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var projectStarCount: UILabel!
    @IBOutlet var project_description: UILabel!
    
    
    var projectVM: ProjectViewModel? {
        didSet {
            if let _name = projectVM?.projectName {
                name.text = _name
            }
            if let starString = projectVM?.starString {
                projectStarCount.text = starString
            }
            if let _description = projectVM?.projectDescription {
                project_description.text = _description
            }
        }
    }

}
