//
//  Author.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

class Author: Decodable {
    var username: String?
    var href:     String?
    var avatar:   String?
}
