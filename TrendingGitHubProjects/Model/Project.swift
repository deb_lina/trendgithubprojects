//
//  Project.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 21/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit

class Project: Decodable {
    var name:               String?
    var description:        String?
    var url:                String?
    var author:             String?
    var language:           String?
    var languageColor:      String?
    var stars:              Int?
    var forks:              Int?
    var currentPeriodStars: Int?
    var builtBy:            [Author]?
}







