//
//  RepoContent.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

struct RepoContent: Decodable {
    var name: String?
    var path: String?
    var sha: String?
    var size: Int?
    var url: String?
    var htmlUrlString: String?
    var gitUrlString: String?
    var downloadUrlString: String?
    var type: String?
    var links: RepoLink?
    
    enum CodingKeys: String, CodingKey {
        case name
        case path
        case sha
        case size
        case url
        case htmlUrlString     = "html_url"
        case gitUrlString      = "git_url"
        case downloadUrlString = "download_url"
        case type
        case links             = "_links"
    }
}

struct RepoLink: Decodable {
    var selfLinkString: String?
    var gitLinkString: String?
    var htmlString: String?
    
    enum CodingKeys: String, CodingKey {
        case selfLinkString = "self"
        case gitLinkString  = "git"
        case htmlString     = "html"
    }
}
