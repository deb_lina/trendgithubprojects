//
//  FetchReadMeAPI.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 23/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

class FetchReadMeAPI {
    
    func call(with url: URL, completion: @escaping ((_ readMeString: String?, _ error: Error?) -> Void)) {
        
        var request = URLRequest(url: url)
        request.setValue("application/vnd.github.3.raw", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                let readMeString = String(data: data, encoding: String.Encoding.utf8)
                completion(readMeString, nil)
            } else if let _error = error {
                completion(nil, _error)
            }
            }.resume()
    }
}
