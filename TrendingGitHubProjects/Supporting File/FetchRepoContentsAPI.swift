//
//  FetchRepoContentsAPI.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 23/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

class FetchRepoContentsAPI {
    
    func call(with url: URL, completion: @escaping ((_ projects: [RepoContent]?, _ error: Error?) -> Void)) {
        
        var request = URLRequest(url: url)
        request.setValue("application/vnd.github.3.raw", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    let decoder = JSONDecoder()
                    let content = try decoder.decode([RepoContent].self, from: data)
                    completion(content, nil)
                } catch let err {
                    print("Err:", err)
                    completion(nil, err)
                }
            } else if let _error = error {
                completion(nil, _error)
            }
            }.resume()
    }
}
