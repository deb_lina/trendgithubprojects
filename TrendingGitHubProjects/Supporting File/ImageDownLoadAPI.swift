//
//  ImageDownLoadAPI.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 23/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

class ImageDownloadAPI {
    
    func call(with imageURL: URL, completion: @escaping ((_ data: Data?, _ error: Error?) -> Void)) {
        URLSession.shared.dataTask(with: imageURL) { (data, response, error) in
            guard error == nil else {
                completion(nil, error!)
                return }
            if let data = data {
                completion(data, nil)
            }
            }.resume()
    }
}
