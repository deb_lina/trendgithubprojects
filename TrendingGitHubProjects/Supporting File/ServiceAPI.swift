//
//  ServiceAPI.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 21/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit

class ServiceAPI {
    
    func call(completion: @escaping ((_ projects: [Project]?, _ error: Error?) -> Void)) {
        
        let baseUrl = Configuration.trendingGitHubURL.value()! as String
        if let url = URL(string: baseUrl + "repositories") {
            
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data {
                    do {
                        let decoder = JSONDecoder()
                        let apps = try decoder.decode([Project].self, from: data)
                        completion(apps, nil)
                    } catch let err {
                        print("Err:", err)
                        completion(nil, err)
                    }
                } else if let _error = error {
                    completion(nil, _error)
                }
                }.resume()
        }
    }
}
