//
//  AlertController.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit

class AlertController {
    class func present(title: String, message: String, okTitle: String, cancelTitle: String, from presentingViewController: UIViewController? = nil, okAction: @escaping () -> Void){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: { _ in okAction() } )
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        (presentingViewController ?? UIWindow.visibleViewController())?.present(alertController, animated: true, completion: nil)
    }
}
