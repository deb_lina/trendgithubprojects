//
//  Color.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static func hoki() -> UIColor {
        return UIColor(red: 96/255.0, green: 125/255.0, blue: 139/255.0, alpha: 1.0)
    }
}
