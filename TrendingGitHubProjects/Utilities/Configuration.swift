//
//  Configuration.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

/** This class has convenience methods and #defines for configuration settings
 Creates and returns the object associated with the configurationKey in the app info.plist.
 @param configurationKey The key to look up.
 @return The configuration object associated with the key.
 */

enum Configuration: String {
    
    case baseUrl           = "Base URL"
    case trendingGitHubURL = "GitHub Trending URL"
    
    /// Creates and returns the object associated with the configurationKey in the app info.plist.
    /// - returns: The configuration object associated with the key.
    
    func value<T>() -> T? {
        let key = self.rawValue
        return getValue(key: key)
    }
    
    private func getValue<T>(key: String) -> T? {
        guard let aValue = Bundle.main.object(forInfoDictionaryKey: key) as? T else {
            let errorMessage = "Configuration setting not found in info.plist for key \(rawValue)."
            assertionFailure(errorMessage)
            return nil
        }
        return aValue
    }
}
