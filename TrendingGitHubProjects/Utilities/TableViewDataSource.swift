//
//  TableViewDataSource.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 23/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit


/* TableViewDataSource is a class for showing an example of the UITableView reusable data source.
 
 ProjectsViewModel is a reusable View Model which can be used for any tableview as the data source.
 
 
 */
class TableViewDataSource: NSObject, UITableViewDataSource {

    private(set) var tableView: UITableView!
    private(set) var cellIdentifier: String!
    private(set) var data: ProjectsViewModel?
    private(set) var designSwapped: Bool!
    
    init(tableViewProperty: TableViewProperty) {
        self.tableView = tableViewProperty._self
        self.data = tableViewProperty.data
        self.cellIdentifier = tableViewProperty.cellIdentifier
        self.designSwapped = tableViewProperty.designSwapped
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let _data = self.data, _data.project_count > 0 else {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No Data Available"
            noDataLabel.textColor     = UIColor.hoki()
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
            return 0
        }
        tableView.separatorStyle = .singleLine
        tableView.backgroundView = nil
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _data = self.data else {
            return 0
        }
        return _data.project_count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        guard let _data = self.data else {
            return cell
        }
        guard let project = _data.pull(at: indexPath.row) else {
            return cell
        }
        if designSwapped == false {
           (cell as! ProjectTableViewCell).projectVM = project
        } else {
            (cell as! ProjectOneTableViewCell).projectVM = project
        }
        return cell
    }
}


