//
//  TableViewProperty.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 23/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation
import UIKit

struct TableViewProperty {
    var _self: UITableView!
    var cellIdentifier: String!
    var data : ProjectsViewModel!
    var designSwapped: Bool? = false
}
