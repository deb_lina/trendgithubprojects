//
//  AuthorViewModel.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation



class AuthorViewModel {
    
    private (set) var author: Author!
    
    init(author: Author) {
        self.author = author
    }
    
    public var authorName: String! { return self.author.username ?? "" }
    public var gitHubURL: URL? { guard let urlString = author.href, let url = URL(string: urlString) else {
        return nil
        }
        return url
    }
    public var avatarImageUrl: URL? { guard let urlString = author.avatar, let url = URL(string: urlString) else {
        return nil
        }
        return url
    }
    
    func loadAvatarImage(imageURL: URL, completion: @escaping ((_ data: Data?, _ error: Error?) -> Void)) {
        ImageDownloadAPI().call(with: imageURL) { (data, error) in
            guard error == nil else {
                completion(nil, error!)
                return }
            if let data = data {
                completion(data, nil)
            }
        }
    }
}
