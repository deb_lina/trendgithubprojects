//
//  ProjectViewModel.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

var contentUrlCache = NSCache<NSString, NSURL>()
var contentCache = NSCache<NSURL, NSString>()

class ProjectViewModel {
    private (set) var project: Project!
    
    private var authors: [AuthorViewModel]?  {
        guard let builtBy = self.project.builtBy, builtBy.count > 0 else {
            return nil
        }
        var _authors = [AuthorViewModel]()
        builtBy.forEach {
            let author = AuthorViewModel(author: $0)
            _authors.append(author)
        }
        return _authors }
    
    
    init(project: Project) {
        self.project = project
    }
    
    public var projectName: String! {return self.project.name ?? ""}
    public var projectDescription: String! { return self.project.description ?? "No Description Available"}
    public var projectURL: URL? { guard let urlString = self.project.url, let url = URL(string: urlString) else {
        return nil
        }
        return url
    }
    public var projectAuthor: String! {return self.project.author ?? ""}
    public var projectLanguage: String! { return self.project.language ?? ""}
    public var starString: String! { guard let count = self.project.stars else {
        return "★ 0 Star"
        }
        return count == 0 ? "★ 0 Star" : count > 1 ? "★ " + "\(count)" + " Stars" : "★ " + "\(count)" + " Star"
    }
    public var forkString: String! { guard let count = self.project.forks else {
        return "⫝ 0 Fork"
        }
        return count == 0 ? "⫝ 0 Fork" : count > 1 ? "⫝ " + "\(count)" + " Forks" : "⫝ " + "\(count)" + " Fork"
    }
    public var builtByCount: Int! { guard let authors = self.authors else { return 0}
        return authors.count
    }
    
    func pullBuiltBy(at index: Int) -> AuthorViewModel? {
        guard let authors = self.authors else { return nil }
        return index < authors.count ? authors[index] : nil
    }
    
    func fetchRepoContents(urlString: String, completion: @escaping ((_ projects: [RepoContent]?, _ error: Error?) -> Void)) {
        if let url = URL(string: urlString) {
            
            FetchRepoContentsAPI().call(with: url) { (repoContents, error) in
                guard error == nil else {
                    completion(nil, error!)
                    return }
                if let repoContents = repoContents {
                    completion(repoContents, nil)
                }
            }
        }
        
    }
    
    func fetchReadMe(url: URL, completion: @escaping ((_ readMeString: String?, _ error: Error?) -> Void)) {
        
        FetchReadMeAPI().call(with: url) { (readMeString, error) in
            guard error == nil else {
                completion(nil, error!)
                return }
            if let readMeString = readMeString {
                completion(readMeString, nil)
            }
        }
    }
    
}
