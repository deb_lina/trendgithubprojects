//
//  ProjectsViewModel.swift
//  TrendingGitHubProjects
//
//  Created by Deblina on 22/01/2019.
//  Copyright © 2019 Deblina. All rights reserved.
//

import Foundation

class ProjectsViewModel {
    
    private (set) var projects: [Project]!
    private var projectsVM: [ProjectViewModel]? {
        guard let _projects = self.projects, _projects.count > 0 else {
            return nil
        }
        var _projectsVM = [ProjectViewModel]()
        _projects.forEach {
            let project = ProjectViewModel(project: $0)
            _projectsVM.append(project)
        }
        return _projectsVM
    }
    
    init(projects: [Project]) {
        self.projects = projects
    }
    
    public var project_count: Int! {
        guard let projectsVM = self.projectsVM, projectsVM.count > 0 else {
            return 0
        }
        return projectsVM.count
    }
    
    func pull(at index: Int) -> ProjectViewModel? {
        guard let projectsVM = self.projectsVM, projectsVM.count > 0 else {
            return nil
        }
        return index < project_count ?  projectsVM[index] : nil
    }
}
